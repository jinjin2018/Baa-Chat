/* Login */
const loginModal = document.querySelector('.login-modal');
const loginForm = document.querySelector('.login-form');
const usernameTextbox = document.querySelector('.username-textbox');
/* Chat */
const messageList = document.querySelector('.message-list');
const textboxContainer = document.querySelector('.textbox-container');
const textbox = document.querySelector('.textbox');
/* Channels */
const roomTitle = document.querySelector('.room-title');
const channel = document.querySelectorAll('.channel');
const userList = document.querySelector('.user-list');

const MESSAGE_LIMIT = 2000;
const USERNAME_LIMIT = 25;
const socket = io();

let currentChannel = channel[0];
let loggedIn = false;
let messageItem = null;
let userItem = null;
let username = null;

/*--------------------------------
            LOGIN
--------------------------------*/

function checkInput(input) {
  const temp = document.createElement('div');
  temp.innerHTML = input;
  return temp.textContent || temp.innerText;
}

loginForm.addEventListener('submit', (e) => {
  e.preventDefault();
  username = checkInput(usernameTextbox.value);
  username = username.replace(/\s\s+/g, ' ');
  if (username === '' || username === ' ') {
    return;
  } else if (loggedIn === true) {
    return;
  } else if (username.length >= USERNAME_LIMIT) {
    alert(`Please keep usernames under ${USERNAME_LIMIT} characters.`);
    return;
  }
  addUser(username);
});

function addUser(username) {
  socket.emit('logIn', username);
  if (hasClass(loginModal, 'hidden')) {
    return;
  }
  loginModal.className += ' hidden';
  loggedIn = true;
  textbox.focus();
  const userJoinInfo = { username, channelName: currentChannel.dataset.name };
  socket.emit('userJoin', (userJoinInfo));
  addUserToLog(username);
}

socket.on('userJoined', (userJoinInfo) => {
  addMessageToLog(`${userJoinInfo.username} joined.`, ' server');
  addUserToLog(userJoinInfo.username);
});

usernameTextbox.focus();

/*--------------------------------
            CHAT
--------------------------------*/

function addMessageToLog(message, type) {
  messageList.scrollTop = messageList.scrollHeight;
  messageItem = document.createElement('li');
  messageItem.setAttribute('class', `message ${type}`);
  messageItem.textContent = message;
  messageList.appendChild(messageItem);
}

function sendMessage() {
  let message = checkInput(textbox.value);
  message = message.replace(/\s\s+/g, ' ');
  if (message === '' || message === ' ') {
    return;
  } else if (loggedIn === false) {
    return;
  } else if (message.length >= MESSAGE_LIMIT) {
    alert(`Please keep messages under ${MESSAGE_LIMIT} characters.`);
    return;
  }
  addMessageToLog(`${username}: ${message}`);
  const messageInfo = { message, username, channel: currentChannel.dataset.name };
  socket.emit('sendMessage', messageInfo);
  textbox.value = null;
  textbox.focus();
}

function hasClass(element, checkAgainstClass) {
  return (` ${element.className} `).indexOf(` ${checkAgainstClass} `) > -1;
}

textboxContainer.addEventListener('submit', (e) => {
  e.preventDefault();
  sendMessage();
});

socket.on('receivedMessage', () => {
  if (hasClass(messageItem, 'sent')) {
    return;
  }
  messageItem.className += ' sent';
});

socket.on('sendCurrentChannel', (messageInfo) => {
  if (currentChannel.dataset.name === messageInfo.channel) {
    addMessageToLog(`${messageInfo.username}: ${messageInfo.message}`, ' sent');
  }
});

/*--------------------------------
            CHANNELS
--------------------------------*/

function changeActiveChannel(e) {
  if (hasClass(e.target.parentNode, 'active')) {
    return;
  }
  currentChannel.className = 'channel';
  currentChannel = e.target.parentNode;
  currentChannel.className += ' active';
  const channelChangeInfo = { username, channelName: currentChannel.dataset.name };
  socket.emit('channelChange', (channelChangeInfo));
}

socket.on('channelChanged', (channelChangeInfo) => {
  /* addUserToLog(channelChangeInfo.username); */
  /* Remove all messages when switching channel.
  while (messageList.firstChild) {
    messageList.removeChild(messageList.firstChild);
  }
  */
  roomTitle.textContent = `${channelChangeInfo.channelName}`;
  addMessageToLog(`Moved to ${channelChangeInfo.channelName} channel.`, ' server');
  messageList.scrollTop = messageList.scrollHeight;
});

for (let i = 0; i < channel.length; i += 1) {
  channel[i].addEventListener('click', (e) => {
    changeActiveChannel(e);
  });
}

function addUserToLog(username, type) {
  userItem = document.createElement('li');
  userItem.setAttribute('class', `user ${type}`);
  userItem.textContent = username;
  userList.appendChild(userItem);
}
