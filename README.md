# Baa Chat

A web-based instant messenger client, hosted on Heroku and built with JavaScript.

Development was halted, expect breakages, poor performance, and for it to steal your favourite cheese - it hasn't been taught what's right or wrong yet, please forgive it.

Thanks to [Alexas Fotos](https://pixabay.com/en/users/Alexas_Fotos-686414/) for the homepage image.

## Screenshots
![Chat.](https://gitlab.com/Baatthew/Baa-Chat/raw/master/img/login.png)
![Chat.](https://gitlab.com/Baatthew/Baa-Chat/raw/master/img/chat.png)
