const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 1337;

app.use(express.static(path.join(__dirname)));

server.listen(port, () => {
  console.log(`Running on http://localhost:${server.address().port}.`);
});

app.get('/', (req, res) => {
  res.send(path.join(__dirname, 'index.html'));
});

io.on('connection', (socket) => {
  socket.on('logIn', (username) => {
    /* Currently no database, join non-specific user channels. */
    socket.join('General');
    socket.join('The Lounge');
  });

  socket.on('userJoin', (userJoinInfo) => {
    socket.to(userJoinInfo.channelName).emit('userJoined', userJoinInfo);
  });

  socket.on('channelChange', (channelChangeInfo) => {
    socket.join(channelChangeInfo.channelName);
    socket.emit('channelChanged', channelChangeInfo);
  });

  socket.on('sendMessage', (messageInfo) => {
    socket.emit('receivedMessage', messageInfo.message);
    socket.to(messageInfo.channel).emit('sendCurrentChannel', messageInfo);
  });

  socket.on('disconnect', () => {
    socket.broadcast.emit('sendCurrentChannel');
  });
});
